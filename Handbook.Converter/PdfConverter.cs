﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using Handbook.Repository;
using iTextSharp.text;
using iTextSharp.text.pdf;
using WpfMath;

namespace Handbook.Converter
{
    public class PdfConverter
    {
        private readonly IDataAccessService context;
        private List<Parameters> listParameters;
        private TexFormulaParser formulaParser;

        public PdfConverter(IDataAccessService context)
        {
            this.context = context;
            listParameters = new List<Parameters>();
            formulaParser = new TexFormulaParser();
        }

        public void SaveDataBaseToPDF(string path)
        {
            LoadParametersFromDatabase();

            if (listParameters.Count == 0)
            {
                throw new ArgumentException("База данных пустая!");
            }

            string ttf = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
            var baseFont = BaseFont.CreateFont(ttf, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
            var font = new Font(baseFont, 14, iTextSharp.text.Font.NORMAL);

            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
            using (var document = new Document(PageSize.A4, 72, 36, 36, 36))
            using (var writer = PdfWriter.GetInstance(document, fs))
            {
                document.Open();

                for (int i = 0; i < 15; i++)
                {
                    document.Add(new Paragraph("\n"));
                }

                string book = "ЭЛЕКТРОННЫЙ СПРАВОЧНИК";
                var titleParagraph = new Paragraph(book, font);
                titleParagraph.Alignment = Element.ALIGN_CENTER;
                titleParagraph.SpacingAfter = 50;
                document.Add(titleParagraph);

                string bookName = "ФИЗИОЛОГИЧЕСКИХ ПОКАЗАТЕЛЕЙ И ИНДЕКСОВ, ПРИГОДНЫХ ДЛЯ ОЦЕНКИ " +
                           "ФСО ВОЕННОСЛУЖАЩИХ В УСЛОВИЯХ АКТИВНЫХ ФИЗИЧЕСКИХ " +
                           "НАГРУЗОК И ПСИХОЭМОЦИОНАЛЬНОГО НАПРЯЖЕНИЯ";
                var nameParagraph = new Paragraph(bookName, font);
                nameParagraph.Alignment = Element.ALIGN_CENTER;
                document.Add(nameParagraph);
                document.NewPage();

                foreach (var item in listParameters)
                {   
                    var title = new Paragraph("\t" + item.Title, font);
                    title.Alignment = Element.ALIGN_JUSTIFIED;
                    title.SpacingAfter = 5;
                    document.Add(title);

                    var titleVariants = new Paragraph("Варианты названий: " + item.Title_variants, font);
                    titleVariants.Alignment = Element.ALIGN_JUSTIFIED;
                    document.Add(titleVariants);

                    var abbreviationVariants = new Paragraph("Варианты аббревиатур: " + item.Abbreviation_variants, font);
                    abbreviationVariants.Alignment = Element.ALIGN_JUSTIFIED;
                    document.Add(abbreviationVariants);

                    var condTitle = new Paragraph("Уникальное название: " + item.Cond_title, font);
                    condTitle.Alignment = Element.ALIGN_JUSTIFIED;
                    document.Add(condTitle);

                    var unit = new Paragraph("Единица измерения: " + item.Unit, font);
                    unit.Alignment = Element.ALIGN_JUSTIFIED;
                    unit.SpacingAfter = 5;
                    document.Add(unit);

                    var description = new Paragraph("Описание показателя\n" + item.Description, font);
                    description.Alignment = Element.ALIGN_JUSTIFIED;
                    description.SpacingAfter = 5;
                    document.Add(description);

                    var applications = new Paragraph("Изучаемые функции и нормативные значения\n" + item.Applications, font);
                    applications.Alignment = Element.ALIGN_JUSTIFIED;
                    applications.SpacingAfter = 5;
                    document.Add(applications);

                    var regulations = new Paragraph("Условия приминения показателя\n" + item.Regulations, font);
                    regulations.Alignment = Element.ALIGN_JUSTIFIED;
                    regulations.SpacingAfter = 5;
                    document.Add(regulations);

                    var methodsContext = context.GetMethods(item);
                    if(methodsContext.Count > 1)
                    {
                        var methodsText = new Paragraph("Методики рассчета показателей", font);
                        methodsText.SpacingAfter = 5;
                        methodsText.Alignment = Element.ALIGN_JUSTIFIED;
                        document.Add(methodsText);
                        var methodParagraph = new Paragraph();
                        methodParagraph.Font = font;
                        foreach (var method in methodsContext)
                        {
                            methodParagraph.Add(method.Method.Title_method + "\n");
                            var renderer = formulaParser.Parse(method.Method.Formula).GetRenderer(TexStyle.Display, 20.0, "Arial");
                            var bitmapSource = renderer.RenderToBitmap(0.0, 0.0);
                            var bitmap = BitmapFromSource(bitmapSource);
                            var image = Image.GetInstance((System.Drawing.Image)bitmap, BaseColor.WHITE);

                            document.Add(image);
                            if (method.Notation != String.Empty && method.Notation != null)
                            {
                                methodParagraph.Add("Примечание: " + method.Notation + "\n");
                            }
                            methodParagraph.Alignment = Element.ALIGN_JUSTIFIED;
                            methodParagraph.SpacingAfter = 5;
                            document.Add(methodParagraph);
                        }

                    }
                    else if(methodsContext.Count == 1)
                    {
                        var methodsText = new Paragraph("Методика рассчета показателя", font);
                        methodsText.SpacingAfter = 5;
                        methodsText.Alignment = Element.ALIGN_JUSTIFIED;
                        document.Add(methodsText);
                        var methodParagraph = new Paragraph();
                        methodParagraph.Font = font;
                        foreach (var method in context.GetMethods(item))
                        {
                            methodParagraph.Add(method.Method.Title_method + "\n");
                            if (method.Notation != String.Empty && method.Notation != null)
                            {
                                methodParagraph.Add("Примечание: " + method.Notation + "\n");
                            }
                            methodParagraph.Alignment = Element.ALIGN_JUSTIFIED;
                            methodParagraph.SpacingAfter = 5;
                            document.Add(methodParagraph);
                        }
                    }

                    var literaturesContext = context.GetLiteratures(item);
                    if(literaturesContext.Count > 0)
                    {
                        var literaturesText = new Paragraph("Список источников", font);
                        var literatures = new List();
                        literatures.Numbered = true;

                        foreach (var literature in literaturesContext)
                        {
                            var listItem = new ListItem(literature.Literature.Lit_title + literature.Pages, font);
                            listItem.Alignment = Element.ALIGN_JUSTIFIED;
                            literatures.Add(listItem);
                        }
                        document.Add(literaturesText);
                        document.Add(literatures);
                    }


                    document.NewPage();
                }
                document.Close();
            }
        }

        private System.Drawing.Bitmap BitmapFromSource(BitmapSource bitmapsource)
        {
            System.Drawing.Bitmap bitmap;
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new System.Drawing.Bitmap(outStream);
            }
            return bitmap;
        }

        private void LoadParametersFromDatabase()
        {
            foreach (var item in context.GetParameters())
            {
                listParameters.Add(item);
            }         
        }
    }
}
