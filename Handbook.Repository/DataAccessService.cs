﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Handbook.Repository.DTO;

namespace Handbook.Repository
{
    public class DataAccessService : IDataAccessService
    {
        public void CreateUpdateLiterature(Literatures literature)
        {
            if (literature == null)
            {
                throw new ArgumentNullException();
            }

            using (var context = new HandbookEntities())
            {
                context.Literatures.AddOrUpdate(literature);
                context.SaveChanges();
            }
        }

        public void CreateUpdateMethod(Methods method)
        {
            if (method == null)
            {
                throw new ArgumentNullException();
            }

            using (var context = new HandbookEntities())
            {
                context.Methods.AddOrUpdate(method);
                context.SaveChanges();
            }
        }

        public void CreateUpdateSystem(Phis_systems system)
        {
            if (system == null)
            {
                throw new ArgumentNullException();
            }

            using (var context = new HandbookEntities())
            {
                context.Phis_systems.AddOrUpdate(system);
                context.SaveChanges();
            }
        }

        public void CreateUpdateParameter(Parameters parameter, 
                                          ObservableCollection<MethodObject> methods, 
                                          ObservableCollection<LiteratureObject> literatures,
                                          ObservableCollection<PhisSystemObject> systems)
        {
            if (parameter == null)
            {
                throw new ArgumentNullException();
            }

            using (var context = new HandbookEntities())
            {
                context.Parameters.Attach(parameter);
                context.Parameters.AddOrUpdate(parameter);

                parameter.Lit_pars.Clear();

                var literatureToDelete = context.Lit_pars.Where(x => x.ID_parameter == parameter.ID_parameter).ToList();
                foreach (var item in literatureToDelete)
                {
                    context.Lit_pars.Remove(item);
                }

                foreach (var item in literatures)
                {
                    var searchItem = context.Literatures.SingleOrDefault(x => x.ID_literature == item.Literature.ID_literature ||
                                                                              x.Lit_title == item.Literature.Lit_title);
                    if (searchItem == null)
                    {
                        searchItem = item.Literature;
                        CreateUpdateLiterature(searchItem);
                        context.Literatures.Attach(searchItem);
                    }
                    var litpars = new Lit_pars() {ID_literature = searchItem.ID_literature, ID_parameter = parameter.ID_parameter, Lit_pages = item.Pages};
                    context.Lit_pars.Add(litpars);
                    parameter.Lit_pars.Add(litpars);
                }

                context.SaveChanges();

                parameter.Met_pars.Clear();

                var methodsToDelete = context.Met_pars.Where(x => x.ID_parameter == parameter.ID_parameter).ToList();
                foreach (var item in methodsToDelete)
                {
                    context.Met_pars.Remove(item);
                }

                foreach (var item in methods)
                {
                    var searchItem = context.Methods.SingleOrDefault(x => x.ID_method == item.Method.ID_method ||
                                                                              x.Title_method == item.Method.Title_method ||
                                                                              x.Formula == item.Method.Formula);
                    if (searchItem == null)
                    {
                        searchItem = item.Method;
                        CreateUpdateMethod(searchItem);
                        context.Methods.Attach(searchItem);
                    }
                    var metpars = new Met_pars() { ID_method = searchItem.ID_method, ID_parameter = parameter.ID_parameter, Notation = item.Notation };
                    context.Met_pars.Add(metpars);
                    parameter.Met_pars.Add(metpars);
                }

                context.SaveChanges();

                parameter.Sys_pars.Clear();

                var systemsToDelete = context.Sys_pars.Where(x => x.ID_parameter == parameter.ID_parameter).ToList();
                foreach (var item in systemsToDelete)
                {
                    context.Sys_pars.Remove(item);
                }

                foreach (var item in systems)
                {
                    var searchItem = context.Phis_systems.SingleOrDefault(x => x.ID_system == item.System.ID_system ||
                                                                             x.Title_system == item.System.Title_system);
                    if (searchItem == null)
                    {
                        searchItem = item.System;
                        CreateUpdateSystem(searchItem);
                        context.Phis_systems.Attach(searchItem);
                    }

                    var syspars = new Sys_pars() { ID_system = searchItem.ID_system, ID_parameter = parameter.ID_parameter};

                    context.Sys_pars.Add(syspars);
                    parameter.Sys_pars.Add(syspars);
                }
                context.SaveChanges();

            }
        }

        public void DeleteLiterature(long idLiterature)
        {
            using (var context = new HandbookEntities())
            {
                var itemToRemove = context.Literatures.SingleOrDefault(x => x.ID_literature == idLiterature);

                if (itemToRemove != null)
                {
                    context.Literatures.Remove(itemToRemove);
                    context.SaveChanges();
                }
            }
        }

        public void DeleteMethod(long idMethod)
        {
            using (var context = new HandbookEntities())
            {
                var itemToRemove = context.Methods.SingleOrDefault(x => x.ID_method == idMethod);

                if (itemToRemove != null)
                {
                    context.Methods.Remove(itemToRemove);
                    context.SaveChanges();
                }
            }
        }

        public void DeleteParameter(long idParameter)
        {
            using (var context = new HandbookEntities())
            {
                var itemToRemove = context.Parameters.SingleOrDefault(x => x.ID_parameter == idParameter);

                if (itemToRemove != null)
                {
                    context.Parameters.Remove(itemToRemove);
                    context.SaveChanges();
                }
            }
        }

        public ObservableCollection<Literatures> GetLiteratures()
        {
            var literatures = new ObservableCollection<Literatures>();

            using (var context = new HandbookEntities())
            {
                foreach (var item in context.Literatures)
                {
                    literatures.Add(item);
                }
            }
            return literatures;
        }

        public ObservableCollection<PhisSystemObject> GetPhisSystems()
        {
            var phisSystems = new ObservableCollection<PhisSystemObject>();

            using (var context = new HandbookEntities())
            {
                foreach (var item in context.Phis_systems)
                {
                    var sysObject = new PhisSystemObject() { System = item };
                    phisSystems.Add(sysObject);
                }
            }
            return phisSystems;
        }

        public ObservableCollection<PhisSystemObject> GetPhisSystems(Parameters parameter)
        {
            using (var context = new HandbookEntities())
            {
                var systems = new ObservableCollection<PhisSystemObject>();
                var localParam = context.Parameters.Attach(parameter);

                foreach (var item in localParam.Sys_pars)
                {
                    var system = context.Phis_systems.SingleOrDefault(x => x.ID_system == item.ID_system);
                    systems.Add(new PhisSystemObject()
                    {
                        System = system
                    });
                }
                return systems;
            }
        }

        public ObservableCollection<LiteratureObject> GetLiteratures(Parameters parameter)
        {
            using (var context = new HandbookEntities())
            {
                var literatures = new ObservableCollection<LiteratureObject>();
                var localParam = context.Parameters.Attach(parameter);

                foreach (var item in localParam.Lit_pars)
                {
                    var literature = context.Literatures.SingleOrDefault(x => x.ID_literature == item.ID_literature);
                    literatures.Add(new LiteratureObject()
                    {
                        Pages = item.Lit_pages,
                        Literature = literature
                    });
                }
                return literatures;
            }
        }

        public Literatures GetLiterature(long idLiterature)
        {
            using (var context = new HandbookEntities())
            {
                var literature = context.Literatures.SingleOrDefault(x => x.ID_literature == idLiterature);
                return literature;
            }
        }

        public ObservableCollection<MethodObject> GetMethods(Parameters parameter)
        {   
            using (var context = new HandbookEntities())
            {
                var methods = new ObservableCollection<MethodObject>();
                var localParam = context.Parameters.Attach(parameter);

                foreach (var item in localParam.Met_pars)
                {
                    var method = context.Methods.SingleOrDefault(x => x.ID_method == item.ID_method);
                    methods.Add(new MethodObject()
                    {
                        Method = method,
                        Notation = item.Notation
                    });
                }
                return methods;
            }
        }

        public Methods GetMethod(long idMethod)
        {
            using (var context = new HandbookEntities())
            {
                var method = context.Methods.SingleOrDefault(x => x.ID_method == idMethod);
                return method;
            }
        }

        public ObservableCollection<Parameters> GetParameters()
        {
            var parameters = new ObservableCollection<Parameters>();
            using (var context = new HandbookEntities())
            {
                foreach (var item in context.Parameters)
                {
                    parameters.Add(item);
                }
            }
            return parameters;
        }

        public ObservableCollection<Parameters> SearchParameters(string searchText)
        {
            throw new NotImplementedException();
        }
    }
}
