﻿using Handbook.Repository;

namespace Handbook.Repository.DTO
{
    public class ParameterObject
    {
        public Parameters Parameter { get; set; }
    }
}
