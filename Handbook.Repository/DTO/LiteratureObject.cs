﻿namespace Handbook.Repository.DTO
{ 
    public class LiteratureObject
    {
        public Literatures Literature { get; set; }
        public string Pages { get; set; }
    }
}
