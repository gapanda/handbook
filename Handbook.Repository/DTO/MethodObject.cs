﻿namespace Handbook.Repository.DTO
{
    public class MethodObject
    {
        public Methods Method { get; set; }
        public string Notation { get; set; }
    }
}
