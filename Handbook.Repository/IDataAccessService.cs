﻿using System.Collections.ObjectModel;
using Handbook.Repository.DTO;

namespace Handbook.Repository
{
    public interface IDataAccessService
    {
        ObservableCollection<Parameters> GetParameters();
        void CreateUpdateParameter(Parameters parameter, 
                                   ObservableCollection<MethodObject> methods, 
                                   ObservableCollection<LiteratureObject> literatures,
                                   ObservableCollection<PhisSystemObject> systems);
        void DeleteParameter(long idParameter);
        ObservableCollection<Parameters> SearchParameters(string searchText);

        ObservableCollection<Literatures> GetLiteratures();
        ObservableCollection<LiteratureObject> GetLiteratures(Parameters parameter);
        Literatures GetLiterature(long idLiterature);
        void CreateUpdateLiterature(Literatures literature);
        void DeleteLiterature(long idLiterature);

        Methods GetMethod(long idMethod);
        ObservableCollection<MethodObject> GetMethods(Parameters parameter);
        void CreateUpdateMethod(Methods method);
        void DeleteMethod(long idMethod);

        void CreateUpdateSystem(Phis_systems method);
        ObservableCollection<PhisSystemObject> GetPhisSystems();
        ObservableCollection<PhisSystemObject> GetPhisSystems(Parameters parameter);
    }
}