﻿using Handbook.Helpers;
using GalaSoft.MvvmLight;
using Handbook.Repository;

namespace Handbook.ViewModel
{
    public class HomeViewModel : ViewModelBase
    {
        private readonly IFrameNavigationService navigationService;
        private readonly IDataAccessService context;

        public HomeViewModel(IFrameNavigationService navigationService, IDataAccessService context)
        {
            this.navigationService = navigationService;
            this.context = context;
        }
    }
}
