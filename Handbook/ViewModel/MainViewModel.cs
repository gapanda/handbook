using System;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Handbook.Repository;
using Handbook.Helpers;
using Handbook.Pages;
using System.Windows.Forms;
using Handbook.Converter;

namespace Handbook.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IFrameNavigationService navigationService;
        private readonly IDataAccessService context;
        private readonly PdfConverter converter;

        private ICommand loadedCommand;
        private ICommand parametersList;
        private ICommand literatureList;
        private ICommand savePDFCommand;

        public ICommand ExitCommand { get; set; }

        public ICommand LoadedCommand
        {
            get
            {
                return loadedCommand
                       ?? (loadedCommand = new RelayCommand(
                           () => { navigationService.NavigateTo(nameof(HomePage)); }));
            }
        }

        public ICommand SavePDFCommand
        {
            get
            {
                return savePDFCommand
                       ?? (savePDFCommand = new RelayCommand(SaveDataBaseToPDF));
            }
        }

        public ICommand ParametersList
        {
            get
            {
                return parametersList
                       ?? (parametersList = new RelayCommand(
                           () => { navigationService.NavigateTo(nameof(ParametersListPage)); }));
            }
        }

        public ICommand LiteratureList
        {
            get
            {
                return literatureList
                       ?? (literatureList = new RelayCommand(
                           () => { navigationService.NavigateTo(nameof(LiteratureListPage)); }));
            }
        }

        public MainViewModel(IFrameNavigationService navigationService, IDataAccessService context,
            PdfConverter converter)
        {
            this.navigationService = navigationService;
            this.context = context;
            this.converter = converter;
            ExitCommand = new RelayCommand(ShutdownApplication);
        }

        private void ShutdownApplication()
        {
            App.Current.Shutdown();
        }

        private void SaveDataBaseToPDF()
        {
            using (var saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Filter = "PDF files(*.pdf)|*.pdf|All files(*.*)|*.*"; ;
                if (saveFileDialog.ShowDialog() == DialogResult.OK && saveFileDialog.FileName != "")
                {
                    try
                    {
                        converter.SaveDataBaseToPDF(saveFileDialog.FileName.ToString());
                    }
                    catch (ArgumentException ex)
                    {
                        MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "������", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
            }
        }
    }
}