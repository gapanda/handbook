/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:Handbook"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using CommonServiceLocator;
using GalaSoft.MvvmLight.Ioc;
using Handbook.Repository;
using Handbook.Helpers;
using System;
using Handbook.Pages;
using GalaSoft.MvvmLight.Messaging;
using Handbook.Converter;

namespace Handbook.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            SetupServices();

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<HomeViewModel>();
            SimpleIoc.Default.Register<ParametersListViewModel>(true);
            SimpleIoc.Default.Register<ParameterModifyViewModel>(true);
            SimpleIoc.Default.Register<LiteratureListViewModel>(true);
            SimpleIoc.Default.Register<AddMethodViewModel>();
            SimpleIoc.Default.Register<AddLiteratureViewModel>();
            SimpleIoc.Default.Register<AddPhisSystemViewModel>();
        }

        private static void SetupServices()
        {
            var navigationService = new FrameNavigationService();
            navigationService.Configure(nameof(HomePage), new Uri("../Pages/HomePage.xaml", UriKind.Relative));
            navigationService.Configure(nameof(ParametersListPage), new Uri("../Pages/ParametersListPage.xaml", UriKind.Relative));
            navigationService.Configure(nameof(ParameterModifyPage), new Uri("../Pages/ParameterModifyPage.xaml", UriKind.Relative));
            navigationService.Configure(nameof(LiteratureListPage), new Uri("../Pages/LiteratureListPage.xaml", UriKind.Relative));
            navigationService.Configure(nameof(AddMethodPage), new Uri("../Pages/AddMethodPage.xaml", UriKind.Relative));
            navigationService.Configure(nameof(AddLiteraturePage), new Uri("../Pages/AddLiteraturePage.xaml", UriKind.Relative));
            navigationService.Configure(nameof(AddPhisSystemPage), new Uri("../Pages/AddPhisSystemPage.xaml", UriKind.Relative));

            SimpleIoc.Default.Register<IFrameNavigationService>(() => navigationService);
            SimpleIoc.Default.Register<IDataAccessService>(() => new DataAccessService());
            SimpleIoc.Default.Register(() => new PdfConverter(SimpleIoc.Default.GetInstance<IDataAccessService>()));
        }

        public MainViewModel Main
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MainViewModel>();
            }
        }

        public HomeViewModel HomeViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<HomeViewModel>();
            }
        }

        public ParametersListViewModel ParameterListViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ParametersListViewModel>();
            }
        }

        public LiteratureListViewModel LiteratureListViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<LiteratureListViewModel>();
            }
        }

        public ParameterModifyViewModel ParameterModifyViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ParameterModifyViewModel>();
            }
        }

        public AddMethodViewModel AddMethodViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AddMethodViewModel>();
            }
        }

        public AddLiteratureViewModel AddLiteratureViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AddLiteratureViewModel>();
            }
        }

        public AddPhisSystemViewModel AddPhisSystemViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AddPhisSystemViewModel>();
            }
        }
        public static void Cleanup()
        {
            SimpleIoc.Default.Unregister<MainViewModel>();
            SimpleIoc.Default.Unregister<HomeViewModel>();
            SimpleIoc.Default.Unregister<ParametersListViewModel>();
            SimpleIoc.Default.Unregister<ParameterModifyViewModel>();
            SimpleIoc.Default.Unregister<LiteratureListViewModel>();
            SimpleIoc.Default.Unregister<AddMethodViewModel>();
            SimpleIoc.Default.Unregister<AddLiteratureViewModel>();
            SimpleIoc.Default.Unregister<AddPhisSystemViewModel>();
            SimpleIoc.Default.Unregister<IDataAccessService>();
            SimpleIoc.Default.Unregister<IFrameNavigationService>();
        }
    }
}