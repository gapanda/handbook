﻿using System;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Handbook.Helpers;
using Handbook.Repository;
using Handbook.Repository.DTO;

namespace Handbook.ViewModel
{
    public class AddLiteratureViewModel : ViewModelBase
    {
        private readonly IFrameNavigationService navigationService;
        private readonly IDataAccessService context;

        public string Title { get; set; }

        public string Pages { get; set; }

        private ICommand acceptCommand;
        private ICommand cancelCommand;

        public AddLiteratureViewModel(IFrameNavigationService navigationService, IDataAccessService context)
        {
            this.navigationService = navigationService;
            this.context = context;
        }

        public ICommand AcceptCommand
        {
            get
            {
                return acceptCommand ?? (acceptCommand = new RelayCommand(() =>
                {
                    if (CheckInputData())
                    {
                        var literature = new LiteratureObject()
                        {
                            Literature = new Literatures() { Lit_title = Title },
                            Pages = Pages
                        };
                        MessengerInstance.Send(literature);
                        Clear();
                        navigationService.GoBack();
                    }
                    else
                    {
                        MessageBox.Show("Неправильно заполнены данные, заполните необходимые поля!", "Ошибка!",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }));
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return cancelCommand ?? (cancelCommand = new RelayCommand(() =>
                {
                    navigationService.GoBack();
                }));
            }
        }

        private bool CheckInputData()
        {
            if (Title == null || Title == String.Empty)
            {
                return false;
            }

            return true;
        }

        private void Clear()
        {
            Title = String.Empty;
            Pages = String.Empty;
        }
    }
}
