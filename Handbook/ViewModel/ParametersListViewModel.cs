﻿using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.CommandWpf;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using Handbook.Helpers;
using Handbook.Repository;
using GalaSoft.MvvmLight.Messaging;
using System.Threading.Tasks;
using Handbook.Repository.DTO;

namespace Handbook.ViewModel
{
    public class ParametersListViewModel : ViewModelBase
    {
        private readonly IFrameNavigationService navigationService;
        private readonly IDataAccessService context;

        private ICommand addCommand;
        private ICommand editCommand;
        private ICommand deleteCommand;
        private ICommand refreshCommand;

        private ObservableCollection<Parameters> parametersList;

        public ParametersListViewModel(IFrameNavigationService navigationService, IDataAccessService context)
        {
            this.navigationService = navigationService;
            this.context = context;
            UpdateList();
        }

        public ObservableCollection<Parameters> ParametersList
        {
            get
            {
                return parametersList;
            }
            set
            {
                parametersList = value;
                RaisePropertyChanged(nameof(ParametersList));
            }
        }

        public Parameters SelectedItem { get; set; }

        public ICommand AddCommand
        {
            get
            {
                return addCommand ?? (addCommand = new RelayCommand(() =>
                           {
                               navigationService.NavigateTo(nameof(ParameterModifyPage));
                           }));
            }
        }

        public ICommand EditCommand
        {
            get
            {
                return editCommand ?? (editCommand = new RelayCommand(() =>
                {
                    if(SelectedItem != null)
                    {
                        navigationService.NavigateTo(nameof(ParameterModifyPage));
                        MessengerInstance.Send(new ParameterObject() { Parameter = SelectedItem });
                    }
                }));
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                return deleteCommand ?? (deleteCommand = new RelayCommand(() =>
                {
                    if (SelectedItem != null)
                    {
                        context.DeleteParameter(SelectedItem.ID_parameter);
                        UpdateList();
                    }
                }));
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                return refreshCommand ?? (refreshCommand = new RelayCommand(() =>
                           {
                               UpdateList();
                           }));
            }
        }

        private void UpdateList()
        {
            ParametersList = context.GetParameters();
        }
    }
}