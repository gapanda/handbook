﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Handbook.Helpers;
using Handbook.Repository;
using Handbook.Repository.DTO;

namespace Handbook.ViewModel
{
    public class AddPhisSystemViewModel : ViewModelBase
    {
        private IFrameNavigationService navigationService;
        private readonly IDataAccessService context;

        private ICommand acceptCommand;
        private ICommand cancelCommand;

        private ObservableCollection<Phis_systems> phisSystemCollection;

        public AddPhisSystemViewModel(IFrameNavigationService navigationService, IDataAccessService context)
        {
            this.navigationService = navigationService;
            this.context = context;
            var collection = context.GetPhisSystems();
            PhisSystemCollection = new ObservableCollection<Phis_systems>();
            AddSystemsToComboBox(collection);
        }

        public ObservableCollection<Phis_systems> PhisSystemCollection
        {
            get { return phisSystemCollection; }
            set
            {
                phisSystemCollection = value;
                RaisePropertyChanged(nameof(PhisSystemCollection));
            }
        }
        public Phis_systems SelectedPhisSystem { get; set; }

        public ICommand AcceptCommand
        {
            get
            {
                return acceptCommand ?? (acceptCommand = new RelayCommand(() =>
                {
                    if(SelectedPhisSystem != null)
                    {
                        var message = new PhisSystemObject() { System = SelectedPhisSystem };
                        MessengerInstance.Send<PhisSystemObject>(message);
                        Clear();
                        navigationService.GoBack();
                    }
                    else
                    {
                        MessageBox.Show("Необходимо выбрать физиологическую систему!");
                    }

                }));
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return cancelCommand ?? (cancelCommand = new RelayCommand(() =>
                {
                    Clear();
                    navigationService.GoBack();
                }));
            }
        }

        private void AddSystemsToComboBox(ObservableCollection<PhisSystemObject> collection)
        {
            foreach(var item in collection)
            {
                PhisSystemCollection.Add(item.System);
            }
            RaisePropertyChanged(nameof(PhisSystemCollection));
        }

        private void Clear()
        {
        }
    }
}
