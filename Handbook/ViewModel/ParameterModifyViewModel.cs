﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Handbook.Repository;
using Handbook.Helpers;
using Handbook.Pages;
using Handbook.Repository.DTO;
using System.Windows;
using System;

namespace Handbook.ViewModel
{
    public class ParameterModifyViewModel : ViewModelBase
    {
        private readonly IFrameNavigationService navigationService;
        private readonly IDataAccessService context;

        private ICommand saveCommand;
        private ICommand cancelCommand;
        private Parameters parameter;
        private ObservableCollection<MethodObject> methodsCollection;
        private ObservableCollection<LiteratureObject> literaturesCollection;
        private ObservableCollection<PhisSystemObject> phisSystemsCollection;

        public ParameterModifyViewModel(IFrameNavigationService navigationService, IDataAccessService context)
        {
            this.navigationService = navigationService;
            this.context = context;

            MessengerInstance.Register<MethodObject>(this, AddMethodToCollection);
            MessengerInstance.Register<LiteratureObject>(this, AddLiteratureToCollection);
            MessengerInstance.Register<ParameterObject>(this, ReceiveParameterMessage);
            MessengerInstance.Register<PhisSystemObject>(this, AddPhisSystemToCollection);

            Parameter = new Parameters();
            MethodsCollection = new ObservableCollection<MethodObject>();
            LiteraturesCollection = new ObservableCollection<LiteratureObject>();
            PhisSystemsCollection = new ObservableCollection<PhisSystemObject>();

        }

        public ICommand AddMethodCommand => new RelayCommand(() => { navigationService.NavigateTo(nameof(AddMethodPage)); });

        public ICommand AddLiteratureCommand => new RelayCommand(() => { navigationService.NavigateTo(nameof(AddLiteraturePage)); });

        public ICommand AddPhisSystemCommand => new RelayCommand(() => { navigationService.NavigateTo(nameof(AddPhisSystemPage)); });

        public ICommand DeleteMethodCommand => new RelayCommand(DeleteMethodFromCollection);

        public ICommand DeleteLiteratureCommand => new RelayCommand(DeleteLiteratureFromCollection);

        public ICommand DeletePhisSystemCommand => new RelayCommand(DeletePhisSystemFromCollection);

        public Parameters Parameter
        {
            get
            {
                return parameter; 

            }
            set
            {
                parameter = value;
                RaisePropertyChanged(nameof(Parameter));
            }
        }

        public MethodObject SelectedMethodObject { get; set; }

        public LiteratureObject SelectedLiteratureObject { get; set; }

        public PhisSystemObject SelectedPhisSystemObject { get; set; }

        public ObservableCollection<MethodObject> MethodsCollection
        {
            get
            {
                return methodsCollection;
            }
            set
            {
                methodsCollection = value;
                RaisePropertyChanged(nameof(MethodsCollection));
            }
        }

        public ObservableCollection<PhisSystemObject> PhisSystemsCollection
        {
            get
            {
                return phisSystemsCollection;
            }
            set
            {
                phisSystemsCollection = value;
                RaisePropertyChanged(nameof(PhisSystemsCollection));
            }
        }

        public ObservableCollection<LiteratureObject> LiteraturesCollection
        {
            get
            {
                return literaturesCollection;
            }
            set
            {
                literaturesCollection = value;
                RaisePropertyChanged(nameof(LiteraturesCollection));
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                return saveCommand ?? (saveCommand = new RelayCommand(() =>
                {
                    try
                    {
                        if (CheckInputData())
                        {
                            context.CreateUpdateParameter(Parameter, MethodsCollection, LiteraturesCollection, PhisSystemsCollection);
                            RefreshPage();
                            navigationService.NavigateTo(nameof(ParametersListPage));
                        }
                        else
                        {
                            MessageBox.Show("Неправильно заполнены данные, заполните необходимые поля!", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }));
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return cancelCommand ?? (cancelCommand = new RelayCommand(() =>
                {
                    RefreshPage();
                    navigationService.NavigateTo(nameof(ParametersListPage));
                }));
            }
        }

        private void ReceiveParameterMessage(ParameterObject message)
        {
            Parameter = message.Parameter;
            var methods = context.GetMethods(Parameter);
            MethodsCollection = methods;
            var literatures = context.GetLiteratures(Parameter);
            LiteraturesCollection = literatures;
            var systems = context.GetPhisSystems(Parameter);
            PhisSystemsCollection = systems;
        }

        private void RefreshPage()
        {
            Parameter = new Parameters();
            MethodsCollection = new ObservableCollection<MethodObject>();
            LiteraturesCollection = new ObservableCollection<LiteratureObject>();
            PhisSystemsCollection = new ObservableCollection<PhisSystemObject>();
        }

        private void AddMethodToCollection(MethodObject methodView)
        {
            MethodsCollection.Add(methodView);
            RaisePropertyChanged(nameof(MethodsCollection));
        }

        private void AddLiteratureToCollection(LiteratureObject literatureView)
        {
            LiteraturesCollection.Add(literatureView);
            RaisePropertyChanged(nameof(LiteraturesCollection));
        }

        private void AddPhisSystemToCollection(PhisSystemObject phisSystem)
        {
            if (!PhisSystemsCollection.Contains(phisSystem))
            {
                PhisSystemsCollection.Add(phisSystem);
            }
            else
            {
                MessageBox.Show("Элемент уже был добавлен!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            RaisePropertyChanged(nameof(PhisSystemsCollection));
        }

        private void DeleteMethodFromCollection()
        {
            MethodsCollection.Remove(SelectedMethodObject);
            RaisePropertyChanged(nameof(MethodsCollection));
        }

        private void DeleteLiteratureFromCollection()
        {
            LiteraturesCollection.Remove(SelectedLiteratureObject);
            RaisePropertyChanged(nameof(LiteraturesCollection));
        }

        private void DeletePhisSystemFromCollection()
        {
            PhisSystemsCollection.Remove(SelectedPhisSystemObject);
            RaisePropertyChanged(nameof(PhisSystemsCollection));
        }

        private bool CheckInputData()
        {
            if (Parameter.Title == null || Parameter.Title == String.Empty ||
                Parameter.Cond_title == null || Parameter.Cond_title == String.Empty ||
                Parameter.Title_variants == null || Parameter.Title_variants == String.Empty ||
                Parameter.Unit == null || Parameter.Unit == String.Empty)
            {
                return false;
            }

            return true;
        }
    }
}