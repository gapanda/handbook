﻿using System;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Handbook.Helpers;
using Handbook.Repository;
using Handbook.Repository.DTO;
using WpfMath;

namespace Handbook.ViewModel
{
    public class AddMethodViewModel : ViewModelBase
    {
        private readonly IFrameNavigationService navigationService;
        private readonly IDataAccessService context;

        private TexFormulaParser formulaParser;

        private ICommand acceptCommand;
        private ICommand cancelCommand;

        private bool hasError = false;
        private string formula = "";

        public bool FormulaHasError
        {
            get { return hasError; }
            set
            {
                hasError = value;
                RaisePropertyChanged(nameof(FormulaHasError));
            }
        }

        public string Title { get; set; }

        public string Formula
        {
            get { return formula; }
            set
            {
                formula = value;
                RaisePropertyChanged(nameof(Formula));
                ParseFormula(value);
            }
        }

        public string Notation { get; set; }

        public AddMethodViewModel(IFrameNavigationService navigationService, IDataAccessService context)
        {
            this.navigationService = navigationService;
            this.context = context;
            formulaParser = new TexFormulaParser();
        }

        public ICommand AcceptCommand
        {
            get
            {
                return acceptCommand ?? (acceptCommand = new RelayCommand(() =>
                {
                    if (CheckInputData())
                    {
                        var method = new MethodObject
                        {
                            Method = new Methods() { Formula = Formula, Title_method = Title },
                            Notation = Notation
                        };
                        MessengerInstance.Send<MethodObject>(method);
                        Clear();
                        navigationService.GoBack();
                    }
                    else
                    {
                        MessageBox.Show("Неправильно заполнены данные, заполните необходимые поля!", "Ошибка!",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }));
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return cancelCommand ?? (cancelCommand = new RelayCommand(() =>
                {
                    Clear();
                    navigationService.GoBack();
                }));
            }
        }

        private void Clear()
        {
            Title = String.Empty;
            Formula = String.Empty;
            Notation = String.Empty;
        }

        private void ParseFormula(string input)
        {
            TexFormula formula = null;
            try
            {
                formula = this.formulaParser.Parse(input);
                FormulaHasError = false;
            }
            catch
            {
                FormulaHasError = true;
            }
        }

        private bool CheckInputData()
        {
            if (FormulaHasError == true || Formula == null || Formula == String.Empty ||
                Title == null || Title == String.Empty)
            {
                return false;
            }

            return true;
        }
    }
}
