﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using Handbook.Helpers;
using Handbook.Repository;

namespace Handbook.ViewModel
{
    public class LiteratureListViewModel : ViewModelBase
    {
        private readonly IFrameNavigationService navigationService;
        private readonly IDataAccessService context;

        private ICommand addCommand;
        private ICommand editCommand;
        private ICommand deleteCommand;
        private ICommand refreshCommand;

        private ObservableCollection<Literatures> literaturesList;

        public LiteratureListViewModel(IFrameNavigationService navigationService, IDataAccessService context)
        {
            this.navigationService = navigationService;
            this.context = context;
        }

        public ObservableCollection<Literatures> LiteraturesList
        {
            get
            {
                return literaturesList;
            }
            set
            {
                literaturesList = value;
                RaisePropertyChanged("LiteraturesList");
            }
        }

        public ICommand AddCommand
        {
            get
            {
                return addCommand ?? (addCommand = new RelayCommand(() =>
                {
                }));
            }
        }

        public ICommand EditCommand
        {
            get
            {
                return editCommand ?? (editCommand = new RelayCommand(() =>
                {
                }));
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                return deleteCommand ?? (deleteCommand = new RelayCommand(() =>
                {
                }));
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                return refreshCommand ?? (refreshCommand = new RelayCommand(() =>
                {
                }));
            }
        }
    }
}
