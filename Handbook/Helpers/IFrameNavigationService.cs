﻿using GalaSoft.MvvmLight.Views;

namespace Handbook.Helpers
{
    public interface IFrameNavigationService: INavigationService
    {
        object Parameter { get; }
    }
}
